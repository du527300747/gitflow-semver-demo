package com.example.gitflowsemverdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitflowSemverDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(GitflowSemverDemoApplication.class, args);
	}

}
